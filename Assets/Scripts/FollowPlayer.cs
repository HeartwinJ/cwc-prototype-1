using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    [SerializeField] private GameObject player;
    private Vector3 cameraOffset = new Vector3(0, 6, -9);

    void Start()
    {
        
    }

    void LateUpdate()
    {
        transform.position = player.transform.position + cameraOffset;
    }
}
